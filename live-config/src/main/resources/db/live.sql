#创建数据库
create database live_db;

#创建用户表
create table auth_user(
id bigint primary key auto_increment comment '主键',
username varchar(20) unique not null comment '用户名',
password char(60) not null comment '密码',
nickname varchar(10) not null comment '昵称',
header_image text not null comment '头像',
email varchar(50) comment '邮箱',
phone char(11) comment '手机',
create_time timestamp not null default now() comment '创建时间',
status tinyint not null default 0 comment '状态',
del_flag bit not null default 0 comment '删除状态 0-可用 1-禁用'
) engine=innodb comment '用户表';

#角色表
create table auth_role(
id bigint primary key auto_increment comment '主键',
role_name varchar(20) unique not null comment '角色名称',
role_tag varchar(20) not null comment '角色别名',
create_time timestamp not null default now() comment '创建时间',
status tinyint not null default 0 comment '状态',
del_flag bit not null default 0 comment '删除状态 0-可用 1-禁用'
) comment '角色表';

#权限表
create table auth_power(
id bigint primary key auto_increment comment '主键',
power_name varchar(20) unique not null comment '权限名称',
power_rest varchar(200) not null comment '资源路径',
create_time timestamp not null default now() comment '创建时间',
status tinyint not null default 0 comment '状态',
del_flag bit not null default 0 comment '删除状态 0-可用 1-禁用'
) comment '权限表';

#用户角色中间表
create table auth_user_role_relation(
uid bigint comment '用户id auth_user表id',
rid bigint comment '角色id auth_role表id',
create_time timestamp not null default now() comment '创建时间',
status tinyint not null default 0 comment '状态',
primary key(uid, rid)
) comment '用户角色关联表';

#角色权限中间表
create table auth_role_power_relation(
rid bigint comment '角色id auth_role表id',
pid bigint comment '权限id auth_power表id',
create_time timestamp not null default now() comment '创建时间',
status tinyint not null default 0 comment '状态',
primary key(rid, pid)
) comment '角色权限关联表';

#插入测试数据
insert into auth_user value(1, 'admin', 'admin', '超级管理员', 'http://www.baidu.com', null, null, now(), 0, 0);
insert into auth_role value(1, '超级管理员', 'ROLE_ADMIN', now(), 0, 0);
insert into auth_user_role_relation value(1,1,now(),0);
insert into auth_power value(1, '测试数据查询权限', '/demo/queryById', now(), 0, 0);
insert into auth_role_power_relation value (1,1, now(), 0);

insert into auth_user value(2, 'test', 'test', '测试账号', 'http://www.baidu.com', null, null, now(), 0, 0);
insert into auth_role value(2, '测试角色', 'ROLE_TEST', now(), 0, 0);
insert into auth_user_role_relation value(2,2,now(),0);

#创建认证客户端表
create table auth_client(
id int primary key auto_increment comment '客户端id',
client_id varchar(20) unique not null comment '客户端名称',
client_secret varchar(60) unique not null comment '客户端密钥(加密)',
access_token_time int not null comment '授权Token的有效期',
refresh_token_time int not null comment '刷新Token的有效期',
auth_grant_type varchar(200) not null comment '设置该客户端的授权模式',
scope varchar(100) default 'all' comment '适用范围',
redirect_url varchar(200) comment '重定向的url',
create_time timestamp not null default now() comment '创建时间',
status tinyint not null default 0 comment '状态'
) comment '认证客户端表';

#测试数据
insert into auth_client value(null, 'pc', 'pc', 3600, 7200, 'password,refresh_token', 'http://www.baidu.com', 'all', now(), 0);

#根据用户名连表查询数据
select u.*, r.id as rid, r.role_name, r.role_tag, p.id as pid, p.power_name, p.power_rest
from auth_user u
         left join auth_user_role_relation ur on u.id = ur.uid
         left join auth_role r on ur.rid = r.id
         left join auth_role_power_relation rp on r.id = rp.rid
         left join auth_power p on p.id = rp.pid
where 1=1
  and u.del_flag = 0
  and u.username = 'admin';

###########演示表结构#############
#测试班级表
create table test_classes (
                              id bigint primary key auto_increment comment '主键',
                              class_name varchar(20) unique not null comment '班级名称',
                              create_time timestamp not null comment '创建时间',
                              update_time timestamp not null comment '最后修改时间',
                              status tinyint default 0 comment '状态',
                              del_flag bit default 0 comment '删除标识 0-启用 1-删除'
) comment '班级表';

#测试学生表
create table test_student (
                              id bigint primary key auto_increment comment '主键',
                              cls_id bigint not null comment '班级id - test_classes表主键',
                              name varchar(20) not null comment '学生姓名',
                              age tinyint not null comment '学生年龄',
                              birthday date comment '生日',
                              phone char(11) comment '手机号',
                              email varchar(30) comment '邮箱',
                              create_time timestamp not null comment '创建时间',
                              update_time timestamp not null comment '最后修改时间',
                              status tinyint default 0 comment '状态',
                              del_flag bit default 0 comment '删除标识 0-启用 1-删除'
) comment '学生表';

#测试数据
insert into test_classes values
(1, '一年一班', now(), now(), 0, 0),
(2, '一年二班', now(), now(), 0, 0),
(3, '一年三班', now(), now(), 0, 0);

insert into test_student values
(1, 1, '小明', 6, now(), '13212345678', '1111@qq.com', now(), now(), 0, 0),
(2, 1, '小红', 7, now(), '13212345678', '1111@qq.com', now(), now(), 0, 0),
(3, 2, '小黑', 5, now(), '13212345678', '1111@qq.com', now(), now(), 0, 0),
(4, 3, '小灰', 8, now(), '13212345678', '1111@qq.com', now(), now(), 0, 0),
(5, 3, '小强', 6, now(), '13212345678', '1111@qq.com', now(), now(), 0, 0),
(6, 3, '小白', 6, now(), '13212345678', '1111@qq.com', now(), now(), 0, 0),
(7, 3, '小黄', 7, now(), '13212345678', '1111@qq.com', now(), now(), 0, 0);
###########演示表结构#############