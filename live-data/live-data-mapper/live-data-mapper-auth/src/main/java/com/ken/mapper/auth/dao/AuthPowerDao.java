package com.ken.mapper.auth.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ken.entity.auth.AuthPower;

/**
 * 权限表(AuthPower)表数据库访问层
 *
 * @author makejava
 * @since 2021-09-12 12:03:34
 */
public interface AuthPowerDao extends BaseMapper<AuthPower> {

}
