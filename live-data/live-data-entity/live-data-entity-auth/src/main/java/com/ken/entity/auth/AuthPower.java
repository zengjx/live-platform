package com.ken.entity.auth;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.ken.entity.base.entity.BaseEntity;
import com.ken.mybatis.annotation.IdAlias;
import lombok.Data;
import lombok.experimental.Accessors;

/**
 * 权限表(AuthPower)表实体类
 *
 * @author makejava
 * @since 2021-09-12 11:27:24
 */
@Data
@Accessors(chain = true)
public class AuthPower extends BaseEntity {

    //主键
    @TableId(type = IdType.AUTO)
    @IdAlias("pid")
    private Long id;
    //权限名称
    @TableField(value = "power_name")
    private String powerName;
    //资源路径
    @TableField(value = "power_rest")
    private String powerRest;
}
