package com.ken.entity.auth.qo;

import com.ken.entity.base.qo.BaseQo;
import lombok.Data;
import lombok.experimental.Accessors;

/**
 * 查询对象
 */
@Data
@Accessors(chain = true)
public class AuthUserQo extends BaseQo {

    /**
     * 用户名
     */
    private String username;
}
