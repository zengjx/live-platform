package com.ken.entity.auth;

import com.ken.entity.base.entity.BaseEntity;
import lombok.Data;
import lombok.experimental.Accessors;

/**
 * 用户角色关联表(AuthUserRoleRelation)表实体类
 *
 * @author makejava
 * @since 2021-09-12 11:27:24
 */
@Data
@Accessors(chain = true)
public class AuthUserRoleRelation extends BaseEntity {
    //用户id auth_user表id
    private Long uid;
    //角色id auth_role表id
    private Long rid;
}
