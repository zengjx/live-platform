package com.ken.entity.auth;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.ken.entity.base.entity.BaseEntity;
import com.ken.mybatis.annotation.ToMore;
import lombok.Data;
import lombok.experimental.Accessors;

import java.util.List;

/**
 * 用户表(AuthUser)表实体类
 *
 * @author makejava
 * @since 2021-09-12 11:27:24
 */
@Data
@Accessors(chain = true)
public class AuthUser extends BaseEntity {
    //主键
    @TableId(type = IdType.AUTO)
    private Long id;
    //用户名
    private String username;
    //密码
    private String password;
    //昵称
    private String nickname;
    //头像
    private String headerImage;
    //邮箱
    private String email;
    //手机
    private String phone;

    /**
     * 角色列表
     */
    @TableField(exist = false)
    @ToMore(type = AuthRole.class)
    private List<AuthRole> roles;

    /**
     * 权限列表
     */
    @TableField(exist = false)
    @ToMore(type = AuthPower.class)
    private List<AuthPower> powers;
}
