package com.ken.data.entity.syslog;

import lombok.Data;
import lombok.experimental.Accessors;
import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.Document;
import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldType;

import java.io.Serializable;
import java.util.Date;
import java.util.Map;

@Data
@Accessors(chain = true)
@Document(indexName = "syslog")
public class SysLog implements Serializable {

    /**
     * 主键
     */
    @Id
    @Field(type = FieldType.Long)
    private String id;

    /**
     * 请求类型
     */
    @Field(type = FieldType.Keyword)
    private String requestType;

    /**
     * 应用名称
     */
    @Field(type = FieldType.Keyword)
    private String appName;

    /**
     * 接口相关信息
     */
    @Field(type = FieldType.Text, analyzer = "ik_max_word")
    private String requestInfo;

    /**
     * 类名称
     */
    @Field(type = FieldType.Keyword)
    private String className;

    /**
     * 方法名
     */
    @Field(type = FieldType.Keyword)
    private String method;

    /**
     * 访问时间
     */
    @Field(type = FieldType.Long)
    private Date createTime;

    /**
     * 访问用户名
     */
    @Field(type = FieldType.Keyword)
    private String username;

    /**
     * 请求参数
     */
    @Field(type = FieldType.Object)
    private Map<String, ?> params;

    /**
     * 返回结果
     */
    @Field(type = FieldType.Keyword)
    private String result;

    /**
     * 异常信息
     */
    @Field(type = FieldType.Keyword)
    private String exception;
}
