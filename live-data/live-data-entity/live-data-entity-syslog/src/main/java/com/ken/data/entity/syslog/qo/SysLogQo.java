package com.ken.data.entity.syslog.qo;

import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

@Data
@Accessors(chain = true)
public class SysLogQo implements Serializable {

    /**
     * 关键字
     */
    private String keyword;
}
