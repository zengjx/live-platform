package com.ken.entity.base.protocol;

import com.ken.mybatis.protocol.BaseResult;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * 接口返回统一对象
 */
@Data
@Accessors(chain = true)
@NoArgsConstructor
@AllArgsConstructor
public class R<T> extends BaseResult<T> implements Serializable {

    /**
     * 返回码
     */
    protected Integer code;

    /**
     * 返回消息
     */
    protected String message;

    /**
     * 数据部分
     */
    private T data;

}
