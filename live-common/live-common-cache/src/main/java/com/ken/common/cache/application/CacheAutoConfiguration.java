package com.ken.common.cache.application;

import com.ken.common.cache.aop.CacheAop;
import com.ken.common.cache.cluster.guava.GuavaMemoryCacheHandler;
import com.ken.common.cache.handler.ClusterCacheHandler;
import com.ken.common.cache.handler.MemoryCacheHandler;
import com.ken.common.cache.memory.redis.RedisClusterCacheHandler;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 * 相关缓存的自动配置
 */
@Configuration
@ConditionalOnProperty(prefix = "kenplugin.cache", value = "enable", havingValue = "true", matchIfMissing = true)
@ComponentScan("com.ken.common.cache")
public class CacheAutoConfiguration {

    @Bean
    @ConditionalOnProperty(prefix = "kenplugin.cache", value = "memoryType", havingValue = "GUAVA", matchIfMissing = true)
    public MemoryCacheHandler getGuavaMemoryCacheHandler(){
        return new GuavaMemoryCacheHandler();
    }


    @Bean
    @ConditionalOnProperty(prefix = "kenplugin.cache", value = "clusterType", havingValue = "REDIS", matchIfMissing = true)
    public ClusterCacheHandler getRedisClusterCacheHandler(){
        return new RedisClusterCacheHandler();
    }


    /**
     * 配置aop
     * @return
     */
    @Bean
    public CacheAop getCacheAop(){
        return new CacheAop();
    }
}
