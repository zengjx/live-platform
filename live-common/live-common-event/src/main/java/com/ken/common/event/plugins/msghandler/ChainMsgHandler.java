package com.ken.common.event.plugins.msghandler;

import com.ken.common.event.framework.handle.MsgHandler;
import com.ken.common.event.framework.message.EventMessage;
import lombok.extern.slf4j.Slf4j;

/**
 * 责任链模式的消息处理
 */
@Slf4j
public class ChainMsgHandler implements MsgHandler {
    @Override
    public void msgHandler(String eventTypeStr, EventMessage eventMsg) {

    }
}
