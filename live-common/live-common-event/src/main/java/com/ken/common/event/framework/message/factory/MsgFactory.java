package com.ken.common.event.framework.message.factory;

import com.ken.common.event.framework.message.EventMessage;

/**
 * 消息实体的工厂对象
 */
public interface MsgFactory {

    <T> EventMessage<T> createMessage(T msg);
}
