package com.ken.common.event.application;

public enum EventMq {

    Rabbit_MQ,
    Rocket_MQ,
    Redis_MQ
}
