package com.ken.common.event.framework.generate.impl;

import com.ken.common.event.framework.generate.MsgIdGenerate;

import java.util.UUID;

/**
 * UUID的
 */
public class UUIDGenerate implements MsgIdGenerate {

    @Override
    public <T> String generate(T msg) {
        return UUID.randomUUID().toString();
    }
}
