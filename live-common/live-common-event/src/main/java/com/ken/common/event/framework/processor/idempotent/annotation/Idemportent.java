package com.ken.common.event.framework.processor.idempotent.annotation;

import com.ken.common.event.framework.processor.idempotent.handler.IdempotentHandler;

import java.lang.annotation.*;

/**
 * 事件的幂等消息处理的标示注解
 */
@Documented
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
public @interface Idemportent {

    Class<? extends IdempotentHandler> value() default IdempotentHandler.class;
}
