package com.ken.common.event.framework.processor.idempotent.handler;

import com.ken.common.event.framework.message.EventMessage;

/**
 * 幂等消息实现类
 */
public interface IdempotentHandler {

    boolean isIdempotent(EventMessage eventMessage);
}
