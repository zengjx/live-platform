package com.ken.common.auth.exception;

import com.ken.entity.base.protocol.R;
import com.ken.entity.base.util.RCode;
import com.ken.entity.base.util.ResultUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.annotation.Order;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@Slf4j
@RestControllerAdvice
@Order(0)
public class AccessExceptionHandler {

    /**
     * 权限不足异常处理
     * @return
     */
    @ExceptionHandler(AccessDeniedException.class)
    public R accessException(AccessDeniedException e){
        log.error("[Access-Exception] - 权限不足！");
        return ResultUtils.createFail(RCode.USER_RIGHTS_FAIL, "对不起，你的权限不足");
    }
}
