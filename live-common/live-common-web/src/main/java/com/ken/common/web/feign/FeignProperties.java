package com.ken.common.web.feign;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

import java.util.List;

@ConfigurationProperties(prefix = "feign")
@Data
public class FeignProperties {

    /**
     * 需要基础传递的参数列表
     */
    private List<String> sendParamNames;
}
