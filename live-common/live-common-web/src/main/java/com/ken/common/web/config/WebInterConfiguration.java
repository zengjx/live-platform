package com.ken.common.web.config;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import java.util.List;
import java.util.Optional;

/**
 * SpringBoot拦截器的配置
 */
@Slf4j
public class WebInterConfiguration implements WebMvcConfigurer {

    @Autowired(required = false)
    private List<HandlerInterceptorAdapter> handlerInterceptorAdapters;

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        log.debug("[Interception - List] - 加载的拦截器 - " + handlerInterceptorAdapters);
        Optional.ofNullable(handlerInterceptorAdapters)
                .ifPresent(handlerInterceptorAdapterList -> {
                    handlerInterceptorAdapterList.forEach(registry::addInterceptor);
                });
    }
}
