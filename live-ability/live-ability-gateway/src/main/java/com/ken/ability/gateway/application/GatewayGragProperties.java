package com.ken.ability.gateway.application;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.stereotype.Component;

import java.util.Map;

/**
 * 灰度发布的配置信息
 */
@Component
@ConfigurationProperties(prefix = "spring.cloud.gateway.gray")
@Data
@RefreshScope
public class GatewayGragProperties {

    /**
     * 是否开启灰度
     */
    private boolean enable;

    /**
     * 灰度的相关服务
     */
    private Map<String, GrayConfig> services;

    /**
     * 灰度的相关配置
     */
    @Data
    public static class GrayConfig{
        //需要灰度的版本
        private String version;
        //需要灰度的比例
        private double rate;
    }

}
