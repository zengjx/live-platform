package com.ken.ability.sentinel.application;

import com.ken.common.sentinel.annotation.EnableSentinelCluster;
import com.ken.common.sentinel.config.SentinelClusterStatus;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
//启动集群流控服务端
@EnableSentinelCluster(SentinelClusterStatus.SERVER)
public class SentinelApplication {

    public static void main(String[] args) {
        SpringApplication.run(SentinelApplication.class, args);
    }
}
