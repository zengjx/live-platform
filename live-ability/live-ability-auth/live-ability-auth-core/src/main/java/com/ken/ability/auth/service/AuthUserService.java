package com.ken.ability.auth.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ken.entity.auth.AuthUser;

/**
 * 用户表(AuthUser)表服务接口
 *
 * @author makejava
 * @since 2021-09-12 12:28:03
 */
public interface AuthUserService extends IService<AuthUser> {

    AuthUser queryAuthUserByUserName(String username);
}
