package com.ken.ability.auth.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ken.ability.auth.service.AuthClientService;
import com.ken.entity.auth.AuthClient;
import com.ken.mapper.auth.dao.AuthClientDao;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;

/**
 * 认证客户端表(AuthClient)表服务实现类
 *
 * @author makejava
 * @since 2021-09-12 11:12:30
 */
@Service
@Slf4j
public class AuthClientServiceImpl extends ServiceImpl<AuthClientDao, AuthClient> implements AuthClientService {

}
