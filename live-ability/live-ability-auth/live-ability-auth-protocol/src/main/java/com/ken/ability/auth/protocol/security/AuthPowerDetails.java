package com.ken.ability.auth.protocol.security;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;

import java.io.Serializable;

@Data
@Slf4j
public class AuthPowerDetails implements Serializable {
    //主键
    private Long id;
    //权限名称
    private String powerName;
    //资源路径
    private String powerRest;
}
