package com.ken.ability.search.service.impl;

import com.ken.ability.search.service.ISysLogService;
import com.ken.data.entity.syslog.SysLog;
import com.ken.data.entity.syslog.qo.SysLogQo;
import lombok.extern.slf4j.Slf4j;
import org.elasticsearch.index.query.MatchAllQueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.elasticsearch.core.ElasticsearchRestTemplate;
import org.springframework.data.elasticsearch.core.IndexOperations;
import org.springframework.data.elasticsearch.core.SearchHit;
import org.springframework.data.elasticsearch.core.SearchHits;
import org.springframework.data.elasticsearch.core.document.Document;
import org.springframework.data.elasticsearch.core.mapping.IndexCoordinates;
import org.springframework.data.elasticsearch.core.query.NativeSearchQuery;
import org.springframework.data.elasticsearch.core.query.Query;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
@Slf4j
public class SysLogServiceImpl implements ISysLogService {

    @Autowired
    private ElasticsearchRestTemplate elasticsearchRestTemplate;

    @Override
    public boolean createIndex() {
        IndexOperations indexOperations = elasticsearchRestTemplate.indexOps(SysLog.class);
        //判断是否存在
        if (!indexOperations.exists()) {
            //不存在则创建
            boolean flag = indexOperations.create();
            log.debug("[Es-Index-Create] 创建索引库syslog - {}", flag);
            return flag;
        }
        log.debug("[Es-Index-Create] 索引库syslog已经存在");
        return false;
    }

    @Override
    public boolean createMapping() {
        IndexOperations indexOperations = elasticsearchRestTemplate.indexOps(SysLog.class);
        Document mapping = indexOperations.createMapping();
        boolean flag = indexOperations.putMapping(mapping);
        log.debug("[Es-Index-Create] 创建索引映射mapping - {}", flag);
        return flag;
    }

    /**
     * 插入系统日志
     * @param sysLog
     * @return
     */
    @Override
    public boolean insertSysLog(SysLog sysLog) {
        sysLog = elasticsearchRestTemplate.save(sysLog);
        log.debug("[Es-Insert-SysLog] 插入系统日志 - {}", sysLog != null);
        return sysLog != null;
    }

    /**
     * 查询系统日志
     * @return
     */
    @Override
    public List<SysLog> querySysLog(SysLogQo sysLogQo) {

        MatchAllQueryBuilder matchAllQueryBuilder = QueryBuilders.matchAllQuery();
        Query query = new NativeSearchQuery(matchAllQueryBuilder);
        SearchHits<SysLog> syslog = elasticsearchRestTemplate.search(query, SysLog.class, IndexCoordinates.of("syslog"));

        //获得查询结果
        List<SearchHit<SysLog>> searchHits = syslog.getSearchHits();
        List<SysLog> sysLogs = new ArrayList<>();
        for (SearchHit<SysLog> searchHit : searchHits) {
            SysLog content = searchHit.getContent();
            sysLogs.add(content);
        }
        return sysLogs;
    }
}
